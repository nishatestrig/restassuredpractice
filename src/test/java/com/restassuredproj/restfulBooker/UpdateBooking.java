package com.restassuredproj.restfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class UpdateBooking {

	public static void main(String[] args) {
		
		RestAssured
		.given().log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/1")
		.contentType(ContentType.JSON)
		.header("Authorization", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
		.body("{\r\n"
				+ "    \"firstname\" : \"Nish\",\r\n"
				+ "    \"lastname\" : \"Brown\",\r\n"
				+ "    \"totalprice\" : 1,\r\n"
				+ "    \"depositpaid\" : true,\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2018-01-01\",\r\n"
				+ "        \"checkout\" : \"2019-01-01\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\" : \"Breakfast\"\r\n"
				+ "}")
		.when()
		.put()
		.then()
		.log()
		.all()
		.assertThat()
		.statusCode(200);
	}
}
