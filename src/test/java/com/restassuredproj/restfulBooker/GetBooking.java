package com.restassuredproj.restfulBooker;

import io.restassured.RestAssured;

public class GetBooking {

	public static void main(String[] args) {

		/*
		 * RequestSpecification requestSecification = RestAssured.given();
		 * 
		 * System.out.println("---print request body---"); requestSecification =
		 * requestSecification.log().all();
		 * requestSecification.baseUri("https://restful-booker.herokuapp.com/");
		 * requestSecification.basePath("booking/{id}");
		 * requestSecification.pathParam("id", "1");
		 * 
		 * // Hit the request and get the reponse Response response =
		 * requestSecification.get();
		 * 
		 * System.out.println("----print response-----"); // validate the response
		 * ValidatableResponse validatableResponse = response.then().log().all();
		 * validatableResponse.statusCode(200);
		 */
		
	//BDD format request	
	RestAssured
		.given().log().all()
			.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("booking/{id}")
			.pathParam("id", "1")
		.when()
			.get()
		.then().log().all()
			.statusCode(200);
		

	}
}
