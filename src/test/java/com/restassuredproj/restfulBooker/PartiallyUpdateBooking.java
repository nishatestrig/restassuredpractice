package com.restassuredproj.restfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PartiallyUpdateBooking {

	public static void main(String[] args) {

		RestAssured.given().log().all().baseUri("https://restful-booker.herokuapp.com/").basePath("booking/1")
				.contentType(ContentType.JSON).header("Authorization", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
				.body("{\r\n" + "    \"firstname\" : \"Nisha\",\r\n" + "    \"lastname\" : \"Thor\"\r\n" + "}").when()
				.patch().then().log().all().assertThat().statusCode(200);

	}
}
