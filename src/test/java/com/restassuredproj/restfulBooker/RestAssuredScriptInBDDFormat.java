package com.restassuredproj.restfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RestAssuredScriptInBDDFormat {

	public static void main(String[] args) {
		
		//Given -build request
		RestAssured
			.given()
			.log().all()
			.baseUri("https://restful-booker.herokuapp.com/").basePath("booking")
			.body("{\r\n" + "    \"firstname\" : \"Nisha\",\r\n" + "    \"lastname\" : \"Thorbole\",\r\n"
				+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : false,\r\n"
				+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2022-04-04\",\r\n"
				+ "        \"checkout\" : \"2022-04-04\"\r\n" + "    },\r\n"
				+ "    \"additionalneeds\" : \"lunch\"\r\n" + "}")
			.contentType(ContentType.JSON)
			.when()
			.post()
			.then().log().all()
			.statusCode(200);

	}
}
