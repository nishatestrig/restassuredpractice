package com.restassuredproj.restfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class CreateBooking {

	public static void main(String[] args) {

		/*
		 * // Build the Request RequestSpecification requestSpecification =
		 * RestAssured.given(); System.out.println("-------print request body-------");
		 * requestSpecification = requestSpecification.log().all();
		 * 
		 * requestSpecification.baseUri("https://restful-booker.herokuapp.com/");
		 * requestSpecification.basePath("booking"); requestSpecification.body("{\r\n" +
		 * "    \"firstname\" : \"Nisha\",\r\n" + "    \"lastname\" : \"Thorbole\",\r\n"
		 * + "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : false,\r\n" +
		 * "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2022-04-04\",\r\n"
		 * + "        \"checkout\" : \"2022-04-04\"\r\n" + "    },\r\n" +
		 * "    \"additionalneeds\" : \"lunch\"\r\n" + "}"); //
		 * requestSpecification.contentType("application/json");
		 * requestSpecification.contentType(ContentType.JSON);
		 * 
		 * // Hit the Request Response response = requestSpecification.post();
		 * 
		 * // Validate the response
		 * System.out.println("-------print response body------"); ValidatableResponse
		 * validatableResponse = response.then().log().all();
		 * validatableResponse.statusCode(200);
		 */

		RestAssured.given().log().all().baseUri("https://restful-booker.herokuapp.com/").basePath("booking")
				.body("{\r\n" + "    \"firstname\" : \"Nisha\",\r\n" + "    \"lastname\" : \"Thorbole\",\r\n"
						+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : false,\r\n"
						+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2022-04-04\",\r\n"
						+ "        \"checkout\" : \"2022-04-04\"\r\n" + "    },\r\n"
						+ "    \"additionalneeds\" : \"lunch\"\r\n" + "}")
				.contentType(ContentType.JSON).post().then().log().all().statusCode(200);

	}

}
